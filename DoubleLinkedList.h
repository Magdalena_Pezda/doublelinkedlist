

#include <iostream>
using namespace std;
#define DOUBLELINKEDLIST_H_


struct Node
{
    int data;
    Node * next;
    Node * previous;

    Node()
    {
    	this->data = 0;
    	this->next = nullptr;
    	this->previous = nullptr;
    }
    Node(const int& d);
    Node(const Node& copyNode);
};


class DoubleLinkedList
{
public:
	DoubleLinkedList (const DoubleLinkedList& LL);
	DoubleLinkedList(); 
	~DoubleLinkedList();  

	void insertToTail(int val); 
	void insertToHead(int val); 
	void insertAfter(int val, Node& node);
	void insertBefore(int val, Node& node); 
	void insertAt(int val, int pos);
	void deleteFromBegin();
	void deleteFromEnd();
	void deleteFromPosition(int pos);
	void deleteNode(Node& node);
	void print();
	void printBackward();
    Node& getNode(int pos);
    int getValue(Node& node); 
    int getValue(int pos);
    void size();
    void swap(int first, int second);
    void swap(Node& first, Node& second);
    void bsort();
    void clear();

private:
    Node * head; //poczatek listy
    Node * tail; //koniec listy (opcjolanie)
    int sizeList;
};


