#include "DoubleLinkedList.h"

Node::Node(const Node& copyNode)
{
	this->data = copyNode.data;
	this->next = copyNode.next;
	this->previous = copyNode.previous;
}

DoubleLinkedList::DoubleLinkedList(const DoubleLinkedList & LL)
{
	if (LL.head == nullptr)
	{
		head = tail = nullptr;
	}
	else
	{
		Node* newHeadTail = new Node;
		newHeadTail->data = LL.head->data;
		this->head = newHeadTail;
		this->tail = newHeadTail;
		this->sizeList = LL.sizeList;

		Node *tempLL = LL.head->next;
		Node* temp = this->head;


		while (tempLL != nullptr)
		{
			Node* newNode = new Node;


			newNode->data = tempLL->data;
			temp->next = newNode;
			newNode->previous = temp;
			newNode->next = nullptr;
			tail = newNode;
			temp = temp->next;
			tempLL = tempLL->next;
		}
	}
}

DoubleLinkedList::DoubleLinkedList()
{
	this->head = nullptr;
	this->tail = nullptr;
	this->sizeList = 0;
}

DoubleLinkedList::~DoubleLinkedList()
{
	delete this;
}

DoubleLinkedList& DoubleLinkedList::operator=(DoubleLinkedList& byValList)
{
	if (byValList.head == nullptr)
		{
			return (*this);
		}
		else
		{
			Node* newHeadTail = new Node;
			newHeadTail->data = byValList.head->data;
			this->head = newHeadTail;
			this->tail = newHeadTail;
			this->sizeList = byValList.sizeList;

			Node *tempLL = byValList.head->next;
			Node* temp = this->head;


			while (tempLL != nullptr)
			{
				Node* newNode = new Node;


				newNode->data = tempLL->data;
				temp->next = newNode;
				newNode->previous = temp;
				newNode->next = nullptr;
				tail = newNode;
				temp = temp->next;
				tempLL = tempLL->next;
			}
		}
}

void DoubleLinkedList::insertToHead(int val)
{
	Node* newNode = new Node;
	newNode->data = val;
	if (head == nullptr)
	{
		head = newNode;
		tail = newNode;
		newNode->next = nullptr;
		newNode->previous = nullptr;
		sizeList++;
	}
	else
	{
		newNode->next = head;
		newNode->previous = nullptr;
		head->previous = newNode;
		head = newNode;
		sizeList++;
	}
}

void DoubleLinkedList::insertToTail(int val)
{
	Node *newNode = new Node;
	newNode->data = val;
	if (tail == nullptr)
	{
		newNode->next = nullptr;
		newNode->previous = nullptr;
		tail = newNode;
		head = newNode;
		sizeList++;
	}
	else
	{
		newNode->previous = tail;
		newNode->next = nullptr;
		tail->next = newNode;
		tail = newNode;
		sizeList++;
	}
}

void DoubleLinkedList::swap(int first, int second)
{
	int counter(1);
	Node* temp1 = head;
	Node* temp2 = head;
	while (counter != first)
	{
		temp1 = temp1->next;
		counter++;
	}
	counter = 1;
	while (counter != second)
	{
		temp2 = temp2->next;
		counter++;
	}
	temp1->data = temp1->data - temp2->data;
	temp2->data = temp2->data + temp1->data;
	temp1->data = temp2->data - temp1->data;
}

void DoubleLinkedList::swap(Node & first, Node & second)
{
//	int temp = first.data;
//	first.data = second.data;
//	second.data = temp;

	first.data = first.data - second.data;
	second.data = second.data + first.data;
	first.data = second.data - first.data;
}

void DoubleLinkedList::clear()
{
	if (sizeList == 0)
	{
		cout << "Your list is empty" << endl;
	}
	else
	{
		Node* temp;
		for (int i = 0 ; i<sizeList; ++i)
		{
			temp = head;
			head = head->next;
			delete temp;
		}
	sizeList = 0;
	}
}

int DoubleLinkedList::getValue(int pos)
{
	int counter(1);
	Node *temp = head;
	if (pos < sizeList)
	{
	while (counter != pos)
	{
		temp = temp->next;
		counter++;
	} cout << "Value on position " << counter << ": " << temp->data << endl;
	}
	else
	{
		cout << "No position" << endl;
	}
}

void DoubleLinkedList::insertBefore(int val, Node & node)
{
	Node* newNode = new Node;
	newNode->data = val;
	sizeList++;

	if (sizeList == 0)
	{
		newNode->next = nullptr;
		newNode->previous = nullptr;
		head = newNode;
		tail = newNode;

	}
	else
	{
		Node * temp = head;
		while (temp != node.previous)
		{
			temp = temp->next;
		}
		newNode->next = temp->next;
		newNode->previous = temp;
		node.previous = newNode;
		temp->next = newNode;
	}
}

void DoubleLinkedList::insertAfter(int val, Node & node)
{
	Node* newNode = new Node;
	newNode->data = val;
	sizeList++;

	Node* temp = head;

	while (temp != node.next)
	{
		temp = temp->next;
	}
	newNode->previous = temp->previous;
	newNode->next = temp;
	temp->previous = newNode;
	node.next = newNode;
}

void DoubleLinkedList::size()
{
	cout << sizeList << endl;
}



int DoubleLinkedList::getValue(Node & node)
{
	return node.data;
}



Node & DoubleLinkedList::getNode(int pos)
{
	int counter(1);
	Node* temp = head;
	if (pos > sizeList)
	{
		cout << "No position at list" << endl;
	}
	for (int i=1;i<sizeList;++i)
	{
		if (pos == i)
		{
			return (*temp);
		}
		else
		{
			temp = temp->next;
		}
	}
}

void DoubleLinkedList::bsort()
{
	if (head == nullptr)
	{
		cout << "List is empty" << endl;
	}
	else
	{
		Node* temp;
		Node* temp1;
		for (int i = 0; i < sizeList; ++i)
		{
			temp = head;
			temp1 = head->next;
			for(int j = 0; j < sizeList-1; ++i)
			{
				if (temp->data <= temp1->data)
				{
					temp = temp->next;
					temp1 = temp1 ->next;
				}
				else
				{
					int n = temp->data;
					temp->data = temp1->data;
					temp1->data = n;
					temp->data = n;
					temp = temp->next;
					temp1 = temp1->next;
//					temp->data = temp->data - temp1->data;
//					temp1->data = temp1->data + temp->data;
//					temp->data = temp1->data - temp->data;
				}
			}
		}
	}
}


Node::Node(const int & d)
{
}

void DoubleLinkedList::printBackward()
{
	Node *temp = tail;

	for (int i = 0; i<sizeList; ++i)
	{
		cout << temp->data << endl;
		temp = temp->previous;
	}
}

void DoubleLinkedList::insertAt(int val, int pos)
{
	if (pos > sizeList)
	{
		cout << "No position at list" << endl;
	}
	insertAfter(val, getNode(pos-1));

//	Node* newNode = new Node;
//	newNode->data = val;
//	Node* nodeBefore = head;
//	Node* nodeAfter = head;
//	Node* actual = head;
//
//	int counter(1);
//	if (counter != pos-1)
//	{
//		nodeBefore = nodeBefore->next;
//	}
//
//	counter = 1;
//	if (counter != pos+1)
//	{
//		nodeAfter = nodeAfter->next;
//	}
//	counter = 1;
//	if (counter != pos)
//	{
//		actual = actual->next;
//	}
//	newNode->next = actual->next;
//	newNode->previous = actual->previous;
//	nodeAfter->previous = newNode;
//	nodeBefore->next = newNode;
//	delete actual;
}

void DoubleLinkedList::deleteFromBegin()
{
	if (head == nullptr)
	{
		cout << "No element to delete" << endl;
	}
	else
	{
	Node* temp = head;
	head = head->next;
	head->previous = nullptr;
	delete temp;
	sizeList--;
	}
}
void DoubleLinkedList::deleteFromEnd()
{
	if (tail == nullptr)
	{
		cout << "No element to delete" << endl;
	}
	else
	{
	Node* temp = tail;
	tail = tail->previous;
	tail->next = nullptr;
	delete temp;
	sizeList--;
	}
}
void DoubleLinkedList::deleteFromPosition(int pos)
{
	if (pos > sizeList)
	{
		cout << "No position" << endl;
	}
	else
	{
		Node* temp = head;
		int counter(1);
		if (counter != pos)
		{
			temp = temp->next;
			counter++;
		}
		Node *nodeBefore = temp->previous;
		Node* nodeAfter = temp->next;

		nodeBefore->next = temp->next;
		nodeAfter->previous = temp->previous;
		delete temp;
		sizeList--;
	}
}

void DoubleLinkedList::deleteNode(Node& node)
{
	Node* temp = &node;
	Node* nodeBefore = node.previous;
	Node* nodeAfter = node.next;

	nodeBefore->next = temp->next;
	nodeAfter->previous = temp->previous;
	delete temp;
	sizeList--;
}

void DoubleLinkedList::print()
{
	Node* temp = head;
	if (sizeList == 0)
	{
		cout << "No elements at list" << endl;
	}
	else
	{
		for (int i = 0; i < sizeList; ++i)
		{
			cout << i+1 << ". " << temp->data << endl;
			temp = temp->next;
		}
	}
}









